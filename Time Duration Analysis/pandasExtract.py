# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 19:35:32 2018

@author: claro
"""

import pandas as pd
#import csv files
OnemonOne = pd.read_csv("1mon1.csv", index_col=0, skip_blank_lines = True)
TwomonOne = pd.read_csv("2mon1.csv", index_col=0, skip_blank_lines = True)
ThreemonOne = pd.read_csv("3mon1.csv", index_col=0, skip_blank_lines = True)
FourmonOne = pd.read_csv("4mon1.csv", index_col=0, skip_blank_lines = True)
#remove NaN cells
OnemonOne = OnemonOne.dropna()
TwomonOne = TwomonOne.dropna()
ThreemonOne = ThreemonOne.dropna()
FourmonOne = FourmonOne.dropna()
#adjust files to master time
TwomonOne['StartSec'] = TwomonOne['StartSec']+97
TwomonOne['EndSec'] = TwomonOne['EndSec']+97
ThreemonOne['StartSec'] = ThreemonOne['StartSec']+3285
ThreemonOne['EndSec'] = ThreemonOne['EndSec']+3285
FourmonOne['StartSec'] = FourmonOne['StartSec']+4558
FourmonOne['EndSec'] = FourmonOne['EndSec']+4558
#concatinate the four files into timeline
timeline = pd.concat([OnemonOne, TwomonOne, ThreemonOne, FourmonOne])
#remove the redundant time data
timeline.drop(timeline.columns[[0, 1]], axis=1, inplace=True)
#add duration column
timeline['duration'] = timeline['EndSec']-timeline['StartSec']
timeline.to_csv('timeline.csv')
