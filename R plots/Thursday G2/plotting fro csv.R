# Set the working directory

# Import packages

library(ggplot2)
library(Rmisc)
library(ggpubr)
library(taRifx)

# Import master dataset

ThursG2 = read.table("ThursG2.csv", sep = ",", header = TRUE)

# Import data tables for each person

p1 = read.table("ThursG21.csv", sep = ",", header = TRUE)
p2 = read.table("ThursG22.csv", sep = ",", header = TRUE)
p3 = read.table("ThursG23.csv", sep = ",", header = TRUE)
p4 = read.table("ThursG24.csv", sep = ",", header = TRUE)
p5 = read.table("ThursG25.csv", sep = ",", header = TRUE)
p6 = read.table("ThursG26.csv", sep = ",", header = TRUE)

# assign variables for each person from the above objects
# assign a y-axis value to each scenario to later create the chart for each scenario
# to aviod a return of levels instead of values, the factors of p1 are converted... 
#...to characters, then to numerics

x1 = as.numeric(as.character(p1$time)) - 0.01
sy1 = as.numeric(as.character(p1$swimming)) - 0.01
fy1 = as.numeric(as.character(p1$flooding)) - 0.01 
qy1 = as.numeric(as.character(p1$quality)) - 0.01
by1 = as.numeric(as.character(p1$biodiversity)) - 0.01 


x1 = as.numeric(as.character(p2$time)) + 0.01
sy2 = as.numeric(as.character(p2$swimming)) + 0.01
fy2 = as.numeric(as.character(p2$flooding)) + 0.01 
qy2 = as.numeric(as.character(p2$quality)) + 0.01
by2 = as.numeric(as.character(p2$biodiversity)) + 0.01 

x1 = as.numeric(as.character(p3$time)) + 0.02
sy3 = as.numeric(as.character(p3$swimming)) + 0.02
fy3 = as.numeric(as.character(p3$flooding)) + 0.02
qy3 = as.numeric(as.character(p3$quality)) + 0.02
by3 = as.numeric(as.character(p3$biodiversity)) + 0.02 

x1 = as.numeric(as.character(p4$time)) - 0.02
sy4 = as.numeric(as.character(p4$swimming)) - 0.02
fy4 = as.numeric(as.character(p4$flooding)) - 0.02
qy4 = as.numeric(as.character(p4$quality)) - 0.02
by4 = as.numeric(as.character(p4$biodiversity)) - 0.02 

x1 = as.numeric(as.character(p5$time)) - 0.03
sy5 = as.numeric(as.character(p5$swimming)) - 0.03
fy5 = as.numeric(as.character(p5$flooding)) - 0.03
qy5 = as.numeric(as.character(p5$quality)) - 0.03
by5 = as.numeric(as.character(p5$biodiversity)) - 0.03 

x1 = as.numeric(as.character(p6$time)) - 0.03
sy6 = as.numeric(as.character(p6$swimming)) - 0.03
fy6 = as.numeric(as.character(p6$flooding)) - 0.03
qy6 = as.numeric(as.character(p6$quality)) - 0.03
by6 = as.numeric(as.character(p6$biodiversity)) - 0.03 

# Use these variables to plot the lines for Swimming
# Color coded by person
# size increased to "1"

sframe1 = data.frame(x=x1, y=sy1, participant='one')          #data.frame created for each participant
sframe2 = data.frame(x=x1, y=sy2, participant='two')
sframe3 = data.frame(x=x1, y=sy3, participant='three')
sframe4 = data.frame(x=x1, y=sy4, participant='four')
sframe5 = data.frame(x=x1, y=sy5, participant='five')
sframe6 = data.frame(x=x1, y=sy6, participant='six')
sDat = rbind(sframe1, sframe2, sframe3, sframe4, sframe5, sframe6)    #data.frames bound for graph            
sDat$participant = as.factor(sDat$participant)                        #participant "number" set as factor

swimming = ggplot(sDat, aes(x=x, y=y, colour=participant)) +
  geom_line(size=1) +
  theme_bw() +                                                                   #BnW theme
  xlab("Minutes") + ylab("Ranking") +                                            #added axis labels
  ggtitle("Swimming") +                                                          #added plot title
  theme(plot.title = element_text(hjust = 0.5)) 

  
#Repeat for Flooding

fframe1 = data.frame(x=x1, y=fy1, participant='one')          #data.frame created for each participant
fframe2 = data.frame(x=x1, y=fy2, participant='two')
fframe3 = data.frame(x=x1, y=fy3, participant='three')
fframe4 = data.frame(x=x1, y=fy4, participant='four')
fframe5 = data.frame(x=x1, y=fy5, participant='five')
fframe6 = data.frame(x=x1, y=fy6, participant='six')
fDat = rbind(fframe1, fframe2, fframe3, fframe4, fframe5, fframe6)    #data.frames bound for graph            
fDat$participant = as.factor(fDat$participant)                        #participant "number" set as factor

flooding = ggplot(fDat, aes(x=x, y=y, colour=participant)) +
  geom_line(size=1) +
  theme_bw() +                                                                   #BnW theme
  xlab("Minutes") + ylab("Ranking") +                                            #added axis labels
  ggtitle("Flooding") +                                                          #added plot title
  theme(plot.title = element_text(hjust = 0.5)) 

#And then Water Quality

qframe1 = data.frame(x=x1, y=qy1, participant='one')          #data.frame created for each participant
qframe2 = data.frame(x=x1, y=qy2, participant='two')
qframe3 = data.frame(x=x1, y=qy3, participant='three')
qframe4 = data.frame(x=x1, y=qy4, participant='four')
qframe5 = data.frame(x=x1, y=qy5, participant='five')
qframe6 = data.frame(x=x1, y=qy6, participant='six')
qDat = rbind(qframe1, qframe2, qframe3, qframe4, qframe5, qframe6)    #data.frames bound for graph            
qDat$participant = as.factor(qDat$participant)                        #participant "number" set as factor

quality = ggplot(qDat, aes(x=x, y=y, colour=participant)) +
  geom_line(size=1) +
  theme_bw() +                                                                   #BnW theme
  xlab("Minutes") + ylab("Ranking") +                                            #added axis labels
  ggtitle("Water Quality") +                                                          #added plot title
  theme(plot.title = element_text(hjust = 0.5)) 

#And finally, Biodiversity

bframe1 = data.frame(x=x1, y=by1, participant='one')          #data.frame created for each participant
bframe2 = data.frame(x=x1, y=by2, participant='two')
bframe3 = data.frame(x=x1, y=by3, participant='three')
bframe4 = data.frame(x=x1, y=by4, participant='four')
bframe5 = data.frame(x=x1, y=by5, participant='five')
bframe6 = data.frame(x=x1, y=by6, participant='six')
bDat = rbind(bframe1, bframe2, bframe3, bframe4, bframe5, bframe6)    #data.frames bound for graph            
bDat$participant = as.factor(bDat$participant)                        #participant "number" set as factor

bio = ggplot(bDat, aes(x=x, y=y, colour=participant)) +
  geom_line(size=1) +
  theme_bw() +                                                                   #BnW theme
  xlab("Minutes") + ylab("Ranking") +                                            #added axis labels
  ggtitle("Biodiversity") +                                                          #added plot title
  theme(plot.title = element_text(hjust = 0.5)) 

#Bring all four plots into the same window

allPlots = ggarrange(swimming, flooding, quality, bio, ncol=1, nrow=4, common.legend = TRUE, legend="right") 

#View the multiplot

allPlots