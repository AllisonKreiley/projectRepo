# This folder contains a Repository intended to store all data collected and analyzed for this current project.
# Any changes made to this folder are updated to Github so that any files with errors can be reverted to their previous state error free.
# The link to the repository's server is provided below
[Server](https://github.com/Darachnid/projectRepo/)
# Please let me know if you have any questions
# Thank you,
# Christopher
